#!/usr/bin/python
"""
Plot w0-wa for a bunch of models.
"""
import numpy as np
import pylab as P
import matplotlib.cm as cm
import matplotlib.patches
import matplotlib.ticker
import scipy.ndimage
import sys
sys.path.append("/home/phil/oslo/bao21cm/")
import baofisher

colours = [ ['#6B6B6B', '#BDBDBD'],
            ['#CC0000', '#F09B9B'],
            ['#1619A1', '#B1C9FD'],
            ['#5B9C0A', '#BAE484'],
            ['#FFB928', '#FFEA28'], ]

nice_colours = ['#CC0000', '#1619A1', '#5B9C0A', '#FFB928', '#750075']

W0MIN = -1.2; W0MAX = -0.7
WAMIN = -0.5; WAMAX = 0.5

def get_centroids(vals):
	"""Get bin centroids"""
	cent = []
	for i in range(len(vals)-1):
		cent.append(0.5*(vals[i]+vals[i+1]))
	return cent

def combine_samples(root, N):
    """
    Load all samples for a given model.
    """
    dat = []
    for i in range(N):
        dat.append( np.load("%s-%d.npy" % (root, N)) )
    return np.concatenate(dat).T

# Load pre-cut data
# 0:w0, 1:wa, 2:h, 3:oq, 4:om, 5:orad, ...
d_mono = np.load("mono.npy")
d_kac = np.load("kac.npy")
d_weyl = np.load("weyl.npy")
d_eft = np.load("eft.npy")
d_axion = np.load("axion.npy")

l_mono = np.load("mono-lambda.npy")
l_kac = np.load("kac-lambda.npy")
l_weyl = np.load("weyl-lambda.npy")
l_eft = np.load("eft-lambda.npy")
l_axion = np.load("axion-lambda.npy")

def get_sig_levels(z):
	"""
	Get values corresponding to the different significance levels for a 
	histo - argument is the histogrammed data
	"""
	linz = z.flatten()
	linz = np.sort(linz)[::-1]
	tot = sum(linz)
	acc = 0.0
	i=-1
	j=0
	#lvls = [0.0, 0.68, 0.95, 0.997, 1.0, 1.01] # Significance levels (Gaussian)
	lvls = [0.0, 0.68, 0.95, 1.0, 1.01] # Significance levels (Gaussian)
	slevels = []
	for item in linz:
		acc += item/tot
		i+=1
		if(acc >= lvls[j] and j < len(lvls)-1):
			print "Reached " + str(j) + "-sigma at", item, "-- index", i
			j+=1
			slevels.append(item)
	slevels.append(-1e-15) # Very small number at the bottom
	return slevels[::-1]



# MONO
#w0 = d_mono[0]; wa = d_mono[1]
#f, x, y = np.histogram2d(w0, wa, bins=150, range=((-1., -0.7), (-0.5, 0.5)))
#smoothed = f.T #scipy.ndimage.gaussian_filter(f.T, 0.2) #f.T
#P.contour(x[:-1], y[:-1], smoothed, levels=get_sig_levels(smoothed), linewidths=1.5, colors=nice_colours[1])
#print "Mono:", w0.shape

# KAC
#w0 = d_kac[0]; wa = d_kac[1]
#f, x, y = np.histogram2d(w0, wa, bins=50, range=((-1., -0.7), (-0.5, 0.5)))
#smoothed = scipy.ndimage.gaussian_filter(f.T, 0.1)
#P.contour(x[:-1], y[:-1], smoothed, levels=get_sig_levels(smoothed), linewidths=1.5, colors=nice_colours[2])

# WEYL
#w0 = d_weyl[0]; wa = d_weyl[1]
#f, x, y = np.histogram2d(w0, wa, bins=50, range=((-1., -0.7), (-0.5, 0.5)))
#smoothed = scipy.ndimage.gaussian_filter(f.T, 1.)
#P.contour(x[:-1], y[:-1], smoothed, levels=get_sig_levels(smoothed), linewidths=2., colors=nice_colours[4])

# AXION
w0 = d_axion[0]; wa = d_axion[1]
f, x, y = np.histogram2d(w0, wa, bins=200, range=((-1., -0.7), (-0.5, 0.5)))
smoothed = f.T #scipy.ndimage.gaussian_filter(f.T, 0.1)
P.contour(x[:-1], y[:-1], smoothed, levels=get_sig_levels(smoothed), linewidths=2., colors=nice_colours[3])
print "Axion:", w0.shape

# EFT
w0 = d_eft[0]; wa = d_eft[1]
f, x, y = np.histogram2d(w0, wa, bins=200, range=((-1., -0.7), (-0.5, 0.5)))
smoothed = f.T #scipy.ndimage.gaussian_filter(f.T, 0.1)
P.contour(x[:-1], y[:-1], smoothed, levels=get_sig_levels(smoothed), linewidths=1.5, colors=nice_colours[0])
print "EFT:", w0.shape

#P.plot(d_eft[0], d_eft[1], 'b.', label="Monomial", alpha=0.5)
#P.plot(l_eft[0], l_eft[1], 'r.', label="Monomial", alpha=0.5)
#P.plot(d_kac[0], d_kac[1], 'y.', label="Kac", alpha=0.5)
#P.plot(d_weyl[0], d_weyl[1], 'g.', label="Weyl", alpha=0.9)
#P.plot(d_eft[0], d_eft[1], 'r.', label="EFT", alpha=0.5)


#x_mono = np.load("ZEQXmono.npy")
#y_mono = np.load("ZEQYmono.npy")
#P.plot(d_mono[0], d_mono[1], 'b.', label="Monomial", alpha=0.2)
#P.plot(x_mono[0], x_mono[1], 'r.', label="Monomial ZEQ", alpha=0.2)
#P.plot(y_mono[0], y_mono[1], 'g.', label="Monomial ZEQ", alpha=0.2)


#P.plot(d_mono[0], d_mono[1], 'r.', alpha=0.2)
#P.plot(d_kac[0], d_kac[1], 'g.', alpha=0.2)
#P.plot(d_weyl[0], d_weyl[1], 'y.', alpha=0.2)
#P.plot(d_eft[0], d_eft[1], 'b.', alpha=0.2)


#cols = cm.jet(dat[2])
#collection = P.scatter(w0, wa, c=h, label="Kac", marker='o', cmap=cm.jet, 
#                       alpha=0.5, lw=0., vmin=0., vmax=1.)
#cbar = P.colorbar(collection)
#cbar.set_label(label="h")

# Plot contours for w0, wa from Euclid
#A,b_HI0,b_HI1,b_HI2,b_HI3,b_HI4,b_HI5,b_HI6,b_HI7,b_HI8,b_HI9,b_HI10,b_HI11,b_HI12,b_HI13, sigma_NL,sigma8,n_s,omegaDE,w0,wa,h,gamma,omega_b

pw0 = 19; pwa = 20
x = -1.; y = 0.
covmat = np.genfromtxt("../bao21cm/euclid_covmat.dat")

#transp = [1., 0.85]
transp = [0.5, 0.3]
w, h, ang, alpha = baofisher.ellipse_for_fisher_params(pw0, pwa, None, Finv=covmat)
ellipses = [matplotlib.patches.Ellipse(xy=(x, y), width=alpha[kk]*w, 
            height=alpha[kk]*h, angle=ang, fc=colours[0][kk], 
            ec=colours[0][0], lw=1.5, alpha=transp[kk]) for kk in [1,0]]
for e in ellipses: P.gca().add_patch(e)
P.plot(x, y, 'kx', markersize=8., mew=1.8)

P.axhline(0., color='k', lw=1.5, alpha=0.2)
P.axvline(-1., color='k', lw=1.5, alpha=0.2)

# Axis labels
P.gca().xaxis.set_major_locator(matplotlib.ticker.MultipleLocator(0.1))
P.gca().xaxis.set_minor_locator(matplotlib.ticker.MultipleLocator(0.05))

# Legend
#labels = [labels[k] for k in range(len(labels))]
#lines = [ matplotlib.lines.Line2D([0.,], [0.,], lw=8.5, color=colours[k][0], alpha=0.65) for k in range(len(labels))]

#P.gcf().legend((l for l in lines), (name for name in labels), prop={'size':'medium'}, bbox_to_anchor=[0.95, 0.95])

P.xlabel("$w_0$", fontdict={'fontsize':'xx-large'}, labelpad=15.)
P.ylabel("$w_a$", fontdict={'fontsize':'xx-large'}, labelpad=15.)
P.xlim((-1.05, -0.75))
P.ylim((-0.5, 0.1))

P.tick_params(axis='both', which='major', labelsize=20, size=8., width=1.5, pad=8.)
P.tick_params(axis='both', which='minor', labelsize=20, size=5., width=1.5, pad=8.)

P.tight_layout()
#P.savefig("pub-w0wa-tracks.pdf", transparent=True)
P.show()








"""
ZOOM = 10
w0 = d_weyl[0]; wa = d_weyl[1]
f, x, y = np.histogram2d(w0, wa, bins=30, range=((-1., -0.7), (-0.5, 0.5)))
smoothed = scipy.ndimage.interpolation.zoom(f.T, ZOOM) #f.T #scipy.ndimage.gaussian_filter(f.T, 2.) # FIXME

xx = np.linspace(np.min(x), np.max(x), (x.size-1)*ZOOM)
yy = np.linspace(np.min(y), np.max(y), (y.size-1)*ZOOM)

print (f.T).shape, smoothed.shape

print smoothed.shape, xx.shape, yy.shape, (f.T).shape, x.shape, y.shape

P.contour(xx, yy, smoothed, linewidths=1.5, colors='g', levels=get_sig_levels(smoothed))
#P.imshow(smoothed, interpolation='none')
P.show()
exit()
"""

##############
## Plot results
#P.subplot(111)
#w0 = d_mono[0]; wa = d_mono[1]
#import scipy.stats
#kde = scipy.stats.gaussian_kde([w0, wa])
#xgrid, ygrid = np.mgrid[-1.:-0.7:80 * 1j,
#                        -0.5:0.5:80 * 1j]
#zvals = np.array(kde.evaluate([xgrid.flatten(), ygrid.flatten()])).reshape(xgrid.shape)

#P.imshow(zvals, extent=[-1., -0.7, 0.5, -0.5])
#P.plot(w0, wa, 'r,')
#P.xlim((-1.1, -0.6))
#P.ylim((-0.6, 0.6))
#P.show()
#print kde
#exit()
##############

