#!/usr/bin/python
"""
Monte Carlo through a load of random realisations of a model.
"""
import numpy as np
#import pylab as P
from potentials import *
#from mpi4py import MPI


BASE_SEED = 10
NSAMP_PER_WORKER = 1000 # No. of samples to return per worker


# Setup MPI
comm = MPI.COMM_WORLD
myid = comm.Get_rank()
size = comm.Get_size()

# Random seed depends on worker
np.random.seed(BASE_SEED + myid)

# Setup integrator
X0 = 1e0 # a0 / a_ref
EFOLDS_BEFORE_EQ = 1e0

# Define model
m = Monomial(A=1., clambda=0., p=2)
#m = Kac(A=1., clambda=1., N=5)

# Initial conditions
pi = 0.1
pdoti = 0.
xi = EFOLDS_BEFORE_EQ * X0 / (1. + m.zeq) # Initial scalefactor
yi = [xi, None, pi, pdoti]

t = np.logspace(-5., 3., 10000)
t = np.concatenate(([0.,], t)) # Add leading zero

#ax1 = P.subplot(211)
#ax2 = P.subplot(212)

vals = []
for i in range(NSAMP_PER_WORKER):
    if i % 50 == 0: print "Model %d from worker %d" % (i, myid)
    
    print i
    
    # Integrate ODEs and plot functions
    y = x, xdot, p, pdot = m.integrate_friedmann(t, yi)
    #ax1.plot(x, xdot/x, 'b-', alpha=0.5)
    
    # Analyse solution
    vals.append( m.analyse(t, y) ) # w0, wa, h, ol, om, orad
    
    # Get new realisation
    m.A = 10.**( np.random.uniform(low=-1., high=1.) )
    m.realise()
    pi = np.random.uniform(low=0., high=2.)
    yi = [xi, None, pi, pdoti]
    
    """
    if w0 == 0. and wa == 0.:
        ax2.plot(w0, wa, 'r.')
    else:
        ax2.plot(w0, wa, 'b.')
    """

print "\t*", myid, "saving results..."
np.save("values-%d" % myid, vals)
comm.barrier()


# Root should combine all the results now
if myid == 0:
    dat = [np.load("values-%d.npy"%i) for i in range(size)]
    all_vals = np.concatenate(dat)
    np.save("values", all_vals)
    print "\t* Saved combined results."
    
comm.barrier()

exit()
# h(a)
ax1.axhline(0.7, color='r')
ax1.axvline(1., color='r')
ax1.set_yscale('log')
ax1.set_xscale('log')
ax1.set_ylim((1e-2, 1e3))
ax1.set_xlim((1e-5, 1e1))

# w(a)
#ax2.axhline(-1., color='r')
#ax2.axhline(+1., color='r')
#ax2.axvline(1., color='r')
#ax2.set_xscale('log')
#ax2.set_ylim((-1.2, 1.2))
#ax2.set_xlim((1e-5, 1e1))

ax2.set_ylim((-20., 20.))
ax2.set_xlim((-2., 2.))


P.show()

exit()
print x[-5:]

P.subplot(211)
P.plot(t, m.hubble(y))
P.axvline(m.t_matter(2.*X0, yi[0]))
P.axhline(0.7)
P.yscale('log')
P.xscale('log')


P.subplot(212)
P.plot(x, m.w(y))
#P.yscale('log')
P.xscale('log')
P.ylim((-1.1, 1.1))


P.show()


exit()


P.subplot(221)
pdot_ana = pdoti / (np.sqrt(3./2.)*pdoti*t + 1.)
#P.plot(t, pdot_ana, 'k-', lw=2.)
P.plot(t, pdot, 'b-')
P.plot(t, pdot2, 'r-')
P.xscale('log')
P.ylabel("pdot(t)")

P.subplot(222)
p_ana = pi + np.sqrt(2./3.)*np.log(1. + np.sqrt(3./2.) * pdoti * t)
#P.plot(t, p_ana, 'k-', lw=2.)
P.plot(t, p, 'b-')
P.plot(t, p2, 'r-')
P.xscale('log')
P.ylabel("p(t)")

P.subplot(223)
x_ana = xi * (1. + np.sqrt(3./2.)*pdoti*t)**(1./3.)
#P.plot(t, x_ana, 'k-', lw=2.)
P.plot(t, x, 'b-')
P.plot(t, x2, 'r-')
P.xscale('log')
P.ylabel("x(t)")

P.subplot(224)
P.plot(x2/X0, xdot2/x2, 'r-')
P.plot(x2/X0, xdot2/x2, 'r-')
P.xscale('log')
P.yscale('log')
P.ylabel("h(a)")

P.xscale('log')

P.show()
