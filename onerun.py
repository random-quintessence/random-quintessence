#!/usr/bin/python
"""
Testing: Do single run and check
"""
import numpy as np
import pylab as P
from potentials import *

np.random.seed(10)
Nsamp = 10

# Setup integrator
X0 = 1e0 # a0 / a_ref (also defined in solve.py)
EFOLDS_BEFORE_EQ = 1e-2

# Define generic priors on potential parameters
m = ShiftSymmEFT2(A=1., clambda=0., Nq=19, pE=6, eF=0.1)

priors = { 'logA':[-1.,1.], 'N':None, 'Nq':None, 'p':None, 'pE':None, 
           'logEF':None, 'zeq':[3200., 3201.] }

# Define time variable (assumed to cover a large enough dynamic range for any model)
t = np.logspace(-2., 3., 10000)
t = np.concatenate(([0.,], t)) # Add leading zero

# Produce set of samples for many randomly-realised models
for i in range(Nsamp):
    # Update potential/initial conditions
    m.realise_scales(**priors)
    xi = EFOLDS_BEFORE_EQ * X0 / (1. + m.zeq) # Initial scalefactor
    pi, pdoti = m.realise_initial(kind='zero')
    yi = [xi, None, pi, pdoti] # xdoti will be calculated from Friedmann eqn.
    
    print "phi_i: ", pi
    print "pE:    ", m.pE
    print "A:     ", m.A
    print "eF:    ", m.eF
    print "\nCoeffs"
    for j in range(20):
        v = "*" if j in [2, 4, m.pE] else ""
        print "%3d   %+4.4f   %+4.4e  %s" % \
              (j, m.coeffs[-1-j], m.coeffs[-1-j]*(pi*m.eF)**j, v) # ShiftSymmEFT2
        #print "%3d   %+4.4f   %+4.4e  %s" % \
        #      (j, m.coeffs[-1-j], m.coeffs[-1-j]*(pi)**j, v) # ShiftSymmEFT
    print ""
    print m.func_V
    print "-"*50
    
    # Integrate ODEs and plot functions
    #y = x, xdot, p, pdot = m.integrate_friedmann(t, yi)
    #P.plot(x, p)
    p = np.linspace(-10., 10., 1000)
    P.plot(p, m.V(p))

#P.xlim((0., 2.))
P.show()
