#!/usr/bin/python
"""
Output dirty sample set (i.e. without cuts applied, but consolidated into one file).
"""
import numpy as np
import pylab as P
import matplotlib.cm as cm
import matplotlib.patches
import sys, os

# Get name from commandline
try:
    NAME = sys.argv[1]
except:
    print "Expected one argument: name (weyl, kac, eft, etc.)"
    sys.exit()


FIX = False
if 'kac-lambda' in NAME: ROWLEN = 44
if 'weyl-lambda' in NAME: ROWLEN = 44
if 'mono-lambda' in NAME: ROWLEN = 44
if 'eft-lambda' in NAME: ROWLEN = 45

def combine_samples(root, basename):
    """
    Load all samples for a given model.
    """
    dat = []
    name = "%s-params" % basename
    for f in os.listdir(root):
        if name in f:
            # Load data
            print "\tLoading %s..." % f
            _dat = np.load("%s/%s" % (root,f))
            # FIXME: Deal with malformed runs
            if FIX:
                dat.append([row if len(row)==ROWLEN else np.zeros(ROWLEN) for row in _dat])
            else:
                dat.append(np.load("%s/%s" % (root,f)))
    return np.concatenate(dat).T

print " * Loading data files"
dat = combine_samples("data/", NAME)
print dat.shape
np.save(NAME+"-dirty", dat)
