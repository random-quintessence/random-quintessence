#!/usr/bin/python
"""
Triangle plot for a bunch of models
"""
import numpy as np
import pylab as P
import matplotlib.cm as cm

def combine_samples(root, N):
    """
    Load all samples for a given model.
    """
    dat = []
    for i in range(N):
        dat.append( np.load("%s-%d.npy" % (root, N)) )
    return np.concatenate(dat).T

#mono1 = combine_samples("data/mono1", 20)
#mono2 = combine_samples("data/mono2", 20)
#mono3 = combine_samples("data/mono3", 20)
#mono4 = combine_samples("data/mono4", 20)

# Mono
#w0, wa, h, oq, om, orad, f_ede,A,N,p,clambda, c0,c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12,c13,c14,c15,c16,c17,c18,c19,c20,c21,c22,c23,c24,c25,c26,c27,c28,c29, phi_i,phidot_i

# EFT
#w0, wa, h, oq, om, orad, f_ede,A,Nq,pE,eF,clambda, c0,c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12,c13,c14,c15,c16,c17,c18,c19,c20,c21,c22,c23,c24,c25,c26,c27,c28,c29, phi_i,phidot_i
#w0,wa,h,oq,om,orad,f_ede,A,Nq,pE,eF,clambda, c0,c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12,c13,c14,c15,c16,c17,c18,c19,c20,c21,c22,c23,c24,c25,c26,c27,c28,c29, phi_i,phidot_i
dat = np.load("eft-tracking-lambda.npy")

print "Omega_M:", np.mean(dat[4])
print "Omega_Q:", np.mean(dat[3])

params = [dat[1], dat[0], dat[2], dat[4], np.log10(dat[7]), dat[8], dat[9], np.log10(dat[10]), np.log10(np.abs(dat[-1]))]
paramnames = ['wa', 'w0', 'h', 'om', 'A', 'Nq', 'pE', 'log(eF)', 'log(phi_i)']
ranges = [ (-1., 0.1), (-1.1, 0.1), (0.55, 0.65), (0.19, 0.23), (-1.1, 1.1), (5, 10), (5, 10), (-3.1, 0.1), (-3., 3.1) ]
Nparam = len(params) # No. of parameters


zz = np.log10(np.abs(dat[-1]))
print np.max(zz), np.min(zz) 


################################################################################
# Setup triangle plot
################################################################################

scale_idx = 1 # Index of experiment to use as reference for setting the x,y scales
nsigma = 4.1 # No. of sigma (of reference experiment 1D marginal) to plot out to

# Set-up triangle plot
fig = P.figure()
axes = [[fig.add_subplot(Nparam, Nparam, (j+1) + i*Nparam) for i in range(j, Nparam)] for j in range(Nparam)]

# Mask-out invalid values
msk = np.where(np.logical_and(params[0] != 0., params[1] != 0.))
print "Accepted:", 1. - float(msk[0].size) / float(params[0].size)

# Fixed width and height for each subplot
w = 1.0 / (Nparam+1.)
h = 1.0 / (Nparam+1.)
l0 = 0.1
b0 = 0.1

# Loop through rows, columns, repositioning plots
# i is column, j is row
for j in range(Nparam):
    for i in range(Nparam-j):
        ax = axes[j][i]
        ii = Nparam - i - 1
        
        # Hide tick labels for subplots that aren't on the main x,y axes
        if j != 0:
            for tick in ax.xaxis.get_major_ticks():
                tick.label1.set_visible(False)
        if i != 0:
            for tick in ax.yaxis.get_major_ticks():
                tick.label1.set_visible(False)
        
        # Fiducial values
        ax.tick_params(axis='both', which='major', labelsize=12)
        ax.tick_params(axis='both', which='minor', labelsize=12)
        
        # Plot scatterplot *or* 1D
        if ii != j:
            print i, j
            
            ax.plot(params[ii][msk], params[j][msk], marker='.', color='b', 
                    alpha=0.5, ls='none')
            ax.set_xlim(ranges[ii])
            ax.set_ylim(ranges[j])
            #ax.set_xlabel(paramnames[i])
            #ax.set_ylabel(paramnames[j])
        else:
            ax.hist(params[ii][msk], bins=50, range=ranges[ii])
            # Match x scale, and hide y ticks
            #if k == scale_idx:
            #    ax.set_xlim((x-nsigma*sig, x+nsigma*sig))
            #ax.tick_params(axis='y', which='both', left='off', right='off')
        
        # Set position of subplot
        pos = ax.get_position().get_points()
        ax.set_position([l0+w*i, b0+h*j, w, h])
        
        if j == 0:
            ax.set_xlabel(paramnames[ii], fontdict={'fontsize':'xx-large'}, labelpad=20.)
        #if i == Nparam-j-1: ax.set_title(label[ii], fontdict={'fontsize':'20'})
        if i == 0:
            ax.set_ylabel(paramnames[j], fontdict={'fontsize':'xx-large'}, labelpad=20.)
            ax.get_yaxis().set_label_coords(-0.2,0.5)

# Add legend
#labels = [labels[k] for k in range(len(labels))]
#lines = [ matplotlib.lines.Line2D([0.,], [0.,], lw=8.5, color=colours[k][0], alpha=0.65) for k in range(len(labels))]

#P.gcf().legend((l for l in lines), (name for name in labels), prop={'size':'xx-large'}, bbox_to_anchor=(-0.15, -0.2, 1, 1))


# Set size and save
P.gcf().set_size_inches(22.,16.)
P.savefig('triangle-eft.png', dpi=100)
P.show()
