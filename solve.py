#!/usr/bin/python
"""
Integrate Raychaudhuri and Klein-Gordon equation for quintessence model.
"""
import numpy as np
import scipy.integrate

X0 = 1e0 # a0 / a_ref
#EFOLDS_BEFORE_EQ = 1e-2

class DEModel(object):
    """
    Class for defining a dark energy model.
    """
    
    def __init__(self, A, zeq=3200., Tcmb=2.725):
        """
        Set-up DE model.
        
        Parameters
        ----------
        
        A : float
            Energy scale of potential, in (M_P M_H)^2 units
        
        zeq : float, optional
            Redshift of matter-radiation equation, as seen from an observer 
            today, a = a_0. (Default: 3200)
        
        Tcmb : float, optional
            CMB temperature (in K) as seen by an observer today, a = a_0.
            (Default: 2.725)
        """
        # Define radiation and matter scales
        self.Tcmb = Tcmb
        self.zeq = zeq
        #self.zeta = 1.349e-6 * (Tcmb)**4. # Reduced radiation density today
        self.zeta = 2.267e-6 * (Tcmb)**4. # Reduced rad. density today (incl. neutrinos)
        self.zeta *= X0**4.
        
        # Scalar field potential
        self.A = A
    
    # Cosmological functions of redshift
    def h(self, y):
        """
        Hubble rate, xdot/x
        """
        x, xdot, p, pdot = y
        return xdot / x
    
    def hubble(self, y):
        """
        Alternative definition of Hubble rate, using the Friedmann equation
        """
        x, xdot, p, pdot = y
        h2 = (self.zeta/3.) * (1. + x*(1. + self.zeq)) / x**4. \
           + ((1./6.) * pdot**2. + self.A*self.V(p)/3.)
        #return np.sign(h2)*np.sqrt(np.abs(h2))
        return np.sqrt(np.abs(h2))
    
    def rhop(self, y):
        """
        Energy density of scalar field
        """
        x, xdot, p, pdot = y
        return 0.5 * pdot**2. + self.A * self.V(p)
    
    def pressurep(self, y):
        """
        Pressure of scalar field
        """
        x, xdot, p, pdot = y
        return 0.5 * pdot**2. - self.A * self.V(p)
    
    def w(self, y):
        """
        Equation of state parameter, w(a)
        """
        return self.pressurep(y) / self.rhop(y)
    
    def wa(self, y):
        """
        First-order term in Taylor expansion of w(a).
        """
        x, xdot, p, pdot = y
        rhop = self.rhop(y)
        return 2.*self.A / (xdot * rhop**2.) * \
             ( 3.*self.V(p)*(xdot/x)*pdot**2. + self.dVdp(p)*pdot*rhop )
    
    def omegap(self, y):
        """
        Fractional energy density in quintessence (includes cosmological 
        constant, if any).
        """
        h = self.hubble(y)
        return self.rhop(y) / (3. * h**2.)
    
    def omegal(self, y):
        """
        Fractional energy density in cosmological constant only (excludes any 
        additional zero-point contribution from the quintessence model).
        """
        h = self.hubble(y)
        return self.rho_lambda / (3. * h**2.)
    
    def omegam(self, y):
        """
        Fractional energy density in matter.
        """
        x, xdot, p, pdot = y
        h = self.hubble(y)
        return (1. + self.zeq)*self.zeta / (3. * h**2.) / x**3.
    
    def omegar(self, y):
        """
        Fractional energy density in radiation.
        """
        x, xdot, p, pdot = y
        h = self.hubble(y)
        return self.zeta / (3. * h**2.) / x**4.
    
    # Analytic results (for comparison)
    def t_matter(self, x, xi):
        """
        Time as a function of x in a matter-dominated Universe.
        """
        t = (2./3.) * np.sqrt(3. / (self.zeta * (1. + self.zeq))) * xi**1.5
        return t * ((x/xi)**1.5 - 1.)
    
    def x_matter(self, t, xi):
        """
        x(t) for a matter-dominated universe.
        """
        return ( (3./2.)*np.sqrt((1.+ self.zeq)*self.zeta/3.) * t + xi**(3./2.) )**(2./3.)
    
    def x_rad(self, t, xi):
        """
        x(t) for a radiation-dominated universe.
        """
        return np.sqrt( 2.*np.sqrt(self.zeta/3.) * t + xi**2. )
    
    def rho_rad(self, y):
        """
        Radiation density.
        """
        x, xdot, p, pdot = y
        return self.zeta * x**(-4.) / 3.
    
    def rho_matter(self, y):
        """
        Matter density.
        """
        x, xdot, p, pdot = y
        return self.zeta * (1. + self.zeq) * x**(-3.) / 3.

    # ODE functions
    def xdot_initial(self, y):
        """
        Initial value of xdot (y[1] == xdot is ignored).
        """
        x, xdot, p, pdot = y
        return x * self.hubble(y)

    def derivs(self, y, t):
        """
        Coupled ODE system (Raychaudhuri and Klein-Gordon equations), broken 
        down into 4 first-order ODEs. The evolution variables are x = adot / a, 
        and p = phi. The time variable is the dimensionless time, t.
        """
        x, xdot, p, pdot = y
        
        # RHS of system
        dx_dt = xdot
        dp_dt = pdot
        dxdot_dt = -(self.zeta / 6.) * (2. + x*(1.+self.zeq)) / x**3. \
                   - (x/3.) * (pdot**2. - self.A*self.V(p))
        dpdot_dt = -3. * pdot * (xdot / x) - self.A*self.dVdp(p)
        return [dx_dt, dxdot_dt, dp_dt, dpdot_dt]
    
    def derivs_friedmann(self, y, t):
        """
        Coupled ODE system (Friedmann and Klein-Gordon equations), broken 
        down into 3 first-order ODEs. The evolution variables are x = adot / a, 
        and p = phi. The time variable is the dimensionless time, t.
        """
        x, xdot, p, pdot = y
        xdot = self.xdot_initial(y) # Update xdot
        
        # RHS of system
        dx_dt = (self.zeta/3.) * (1. + x*(1.+self.zeq))/x**2. \
              + (x**2. / 3.) * (pdot**2. / 2. + self.A * self.V(p))
        #dx_dt = np.sign(dx_dt)*np.sqrt(np.abs(dx_dt)) # Deal with turnovers (FIXME)
        dx_dt = np.sqrt(np.abs(dx_dt))
        dp_dt = pdot
        dpdot_dt = -3. * pdot * (xdot / x) - self.A*self.dVdp(p)
        dxdot_dt = 0. # Ignore (not used)
        
        return [dx_dt, dxdot_dt, dp_dt, dpdot_dt]

    def integrate(self, t, yi):
        """
        Integrate ODE system (x, xdot, p, pdot).
        """
        yi[1] = self.xdot_initial(yi) # Use Friedmann to set initial xdot
        y = scipy.integrate.odeint(self.derivs, yi, t).T
        return y
    
    def integrate_friedmann(self, t, yi):
        """
        Integrate ODE system (x, xdot, p, pdot).
        """
        y = scipy.integrate.odeint(self.derivs_friedmann, yi, t).T
        y[1] = self.xdot_initial(y) # Update Friedmann eqn.
        return y
    
    def ede_fraction(self, y, z_lss=1090.):
        """
        Calculate fraction of density in dark energy at early times (at last 
        scattering).
        """
        x, xdot, p, pdot = y
        a = x/X0
        a[np.isnan(a)] = -1.
        a_lss = 1./(1. + z_lss)
        
        # Get index nearest to a_lss
        if np.max(a) >= a_lss:
            idx = np.argmin(np.abs(a - a_lss))
            ylss = y.T[idx]
            oq = self.omegap(ylss)
            om = self.omegam(ylss)
            orad = self.omegar(ylss)
            return 1. - (om + orad)
        else:
            return np.nan
    
    def analyse(self, t, y):
        """
        Analyse solution; find where x0, t0 are (if x0 was even reached), and 
        what various values of comsological functions are at that time.
        """
        x, xdot, p, pdot = y
        a = x/X0
        hh = xdot / x
        a[np.isnan(a)] = -1.
        hh[np.isnan(hh)] = 1e100
        
        # Get index nearest a0
        if np.max(a) >= 1.:
            idx = np.argmin((a - 1.)**2.)
            ytoday = y.T[idx]
            oq = self.omegap(ytoday)
            om = self.omegam(ytoday)
            orad = self.omegar(ytoday)
            occ = self.omegal(ytoday)
            f_ede = self.ede_fraction(y)
            zeq = self.zeq
            return self.w(ytoday), self.wa(ytoday), self.hubble(ytoday), \
                   oq, occ, om, orad, f_ede, zeq
        else:
            return np.zeros(9)
