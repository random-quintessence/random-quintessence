#!/usr/bin/python
"""
Calculate relative entropy for a given DE model.
"""
import numpy as np
import pylab as P

NBINS = 1000 # Need high resolution for small FOM
WAMAX = 5. #30. # Prior range on w_a (larger encompasses more of the range of models)
W0MIN = -2.
W0MAX = 1.
THETA = 90. * np.pi/180. # Rotation angle for (w0,wa) covmat

#W0 = -1.;   WA = 0.    # LCDM
#W0 = -0.95; WA = 0.    # Euclid fiducial model
W0 = -0.95; WA = -0.07 # Degeneracy-direction non-LCDM model

# Load Fisher matrix for Euclid
pw0 = 19; pwa = 20
covmat = np.genfromtxt("../bao21cm/euclid_covmat.dat")
c11 = covmat[pw0,pw0]
c22 = covmat[pwa,pwa]
c12 = covmat[pw0,pwa]
cov0 = np.array([[c11, c12], [c12, c22]])

# Rotate covariance matrix
R = np.array( [[np.cos(THETA), -np.sin(THETA)], [np.sin(THETA), np.cos(THETA)]] )
print R

def get_sig_levels(z, quiet=True):
	"""
	Get values corresponding to the different significance levels for a 
	histo - argument is the histogrammed data
	"""
	linz = z.flatten()
	linz = np.sort(linz)[::-1]
	tot = sum(linz)
	acc = 0.0
	i=-1
	j=0
	#lvls = [0.0, 0.68, 0.95, 0.997, 1.0, 1.01] # Significance levels (Gaussian)
	lvls = [0.0, 0.68, 0.95, 1.0, 1.01] # Significance levels (Gaussian)
	slevels = []
	for item in linz:
		acc += item/tot
		i+=1
		if(acc >= lvls[j] and j < len(lvls)-1):
			if not quiet: print "Reached " + str(j) + "-sigma at", item, "-- index", i
			j+=1
			slevels.append(item)
	slevels.append(-1e-15) # Very small number at the bottom
	return slevels[::-1]


def loglike(x, y, x0, y0, cov):
    """
    Gaussian likelihood for 2 parameters (w0, wa).
    """
    #Cinv = np.linalg.inv(cov)
    Cinv = np.dot(R, np.dot(np.linalg.inv(cov), R.T)) # Rotate covmat
    xx = np.atleast_2d([x - x0, y - y0])
    
    logL = -0.5 * np.einsum("imn,imn->mn", xx, np.einsum("ij,ikl", Cinv, xx))
    return logL - np.log(2.*np.pi * np.sqrt(np.linalg.det(cov)))

def relative_entropy(logL, q):
    """
    Calculate relative entropy, dS = log A + Sum_k (A L_k q_k log L_k),
    where A is the normalisation for the posterior P_k = A L_k q_k, and 
    L_k is the likelihood and q_k is the prior density.
    """
    Q = 1. / np.sum(q) # Prior normalisation
    Pk = np.exp(logL) * (Q * q)
    A = 1. / np.sum(Pk) # Posterior normalisation
    dS = A * np.sum(Pk * logL) + np.log(A)
    return dS

def rel_entropy_for_fom(fom, cov0, w0, wa, uniform=False):
    """
    Get relative entropy for a given FOM.
    """
    # Scale covmat for desired FOM
    fom0 = 1. / np.sqrt(np.linalg.det(cov0))
    cov = cov0 * (fom0 / fom)

    # Histogram prior points (and get bin centroids)
    q, x, y = np.histogram2d(w0, wa, bins=NBINS, range=((W0MIN, W0MAX), (-WAMAX, WAMAX)))
    q = q.T
    if uniform: q = np.ones(q.shape) # Assume prior is actually uniform
    xc = [0.5*(x[i+1] + x[i]) for i in range(x.size-1)]
    yc = [0.5*(y[i+1] + y[i]) for i in range(y.size-1)]

    # Calculate likelihood and posterior normalisation
    X, Y = np.meshgrid(xc, yc)
    logL = loglike(X, Y, W0, WA, cov)

    # Calculate relative entropy
    dS = relative_entropy(logL, q)
    """
    P.contour(xc, yc, q, linewidths=1., levels=get_sig_levels(q))
    P.contour(xc, yc, np.exp(logL), linewidths=1., levels=get_sig_levels(q))
    P.xlim((-2., 0.))
    P.ylim((-1., 1.))
    P.show()
    exit()
    """
    return dS

fom0 = 1. / np.sqrt(np.linalg.det(cov0))
fom = np.logspace(0., 4., 14)
names = ["mono", "kac", "weyl", "eft", "axion", "extradim"] # ZERO, NO LAMBDA, CUT

for nm in names:
    # Load (w0, wa) data for given model and calculate dS as fn. of FOM
    print "Model:", nm
    w0, wa = np.load("%s-lambda.npy" % nm)[:2]
    dS = [rel_entropy_for_fom(ff, cov0, w0, wa) for ff in fom]
    P.plot(fom, dS, label=nm, lw=1.5, marker='o')

# Calculate dS for uniform prior
dS = np.array([rel_entropy_for_fom(ff, cov0, w0, wa, uniform=True) for ff in fom])
P.plot(fom, dS - dS[0], label="Uniform - $\Delta S_0$", lw=1.5, marker='o', color='k')

P.axvline(fom0, ls='dotted', color='k', lw=1.3) # Euclid FOM
P.axhline(0., ls='dotted', color='k', lw=1.3)

P.xscale('log')
P.ylim((-0.1, 3.))

P.title("Fiducial $w_0=%3.2f$, $w_a=%3.2f$" % (W0, WA))

P.legend(loc='upper left', ncol=2)
P.xlabel("FOM", fontsize=20.)
P.ylabel("$\Delta S$", fontsize=20.)
P.tight_layout()
P.show()
