#!/usr/bin/python
"""
Plot w0-wa for a bunch of models.
"""
import numpy as np
import pylab as P
import matplotlib.cm as cm
import matplotlib.patches
import matplotlib.ticker
import scipy.ndimage
import sys
sys.path.append("/home/phil/oslo/bao21cm/")
import baofisher

colours = [ ['#6B6B6B', '#BDBDBD'],
            ['#CC0000', '#F09B9B'],
            ['#1619A1', '#B1C9FD'],
            ['#5B9C0A', '#BAE484'],
            ['#FFB928', '#FFEA28'], ]

nice_colours = ['#CC0000', '#1619A1', '#5B9C0A', '#FFB928', '#750075']

W0MIN = -1.2; W0MAX = -0.7
WAMIN = -0.5; WAMAX = 0.5

def get_centroids(vals):
	"""Get bin centroids"""
	cent = []
	for i in range(len(vals)-1):
		cent.append(0.5*(vals[i]+vals[i+1]))
	return cent

def combine_samples(root, N):
    """
    Load all samples for a given model.
    """
    dat = []
    for i in range(N):
        dat.append( np.load("%s-%d.npy" % (root, N)) )
    return np.concatenate(dat).T

# Load pre-cut data
# 0:w0, 1:wa, 2:h, 3:oq, 4:om, 5:orad, ...
d_mono = np.load("mono.npy")
d_kac = np.load("kac.npy")
d_weyl = np.load("weyl.npy")
d_eft = np.load("eft.npy")
d_axion = np.load("axion.npy")

l_mono = np.load("mono-lambda.npy")
l_kac = np.load("kac-lambda.npy")
l_weyl = np.load("weyl-lambda.npy")
l_eft = np.load("eft-lambda.npy")
l_axion = np.load("axion-lambda.npy")


# Plot results
P.subplot(111)

#x_mono = np.load("ZEQmono.npy")
#P.plot(d_mono[0], d_mono[1], 'b.', label="Monomial", alpha=0.5)
#P.plot(x_mono[0], x_mono[1], 'r.', label="Monomial ZEQ", alpha=0.5)

# w0,wa,h,oq,ol,om,orad,f_ede,zeq,A,N,eF,eNP,clambda,
# c0,c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12,c13,c14,c15,c16,c17,c18,c19,c20,
# c21,c22,c23,c24,c25,c26,c27,c28,c29,phi_i,phidot_i
y = d_mono
w0 = y[0]; wa = y[1]; h = y[3]
collection = P.scatter(w0, wa, c=h, label="Mono", marker='.', cmap=cm.jet, 
                       alpha=0.5, lw=0., edgecolor=cm.jet((h-0.6)/0.2), vmin=0.6, vmax=0.8)
cbar = P.colorbar(collection)
#cbar.set_label(label="$\Omega_Q$")
cbar.ax.set_ylabel("$\Omega_Q$", fontdict={'size':'x-large'}, labelpad=10.)

print "h:", np.mean(h), np.min(h), np.max(h), np.std(h)


P.axhline(0., color='k', lw=1.5, alpha=0.2)
P.axvline(-1., color='k', lw=1.5, alpha=0.2)

# Axis labels
P.gca().xaxis.set_major_locator(matplotlib.ticker.MultipleLocator(0.1))
P.gca().xaxis.set_minor_locator(matplotlib.ticker.MultipleLocator(0.05))

# Legend
#labels = [labels[k] for k in range(len(labels))]
#lines = [ matplotlib.lines.Line2D([0.,], [0.,], lw=8.5, color=colours[k][0], alpha=0.65) for k in range(len(labels))]

#P.gcf().legend((l for l in lines), (name for name in labels), prop={'size':'medium'}, bbox_to_anchor=[0.95, 0.95])

P.xlabel("$w_0$", fontdict={'fontsize':'xx-large'}, labelpad=15.)
P.ylabel("$w_a$", fontdict={'fontsize':'xx-large'}, labelpad=15.)
P.xlim((-1.05, -0.75))
P.ylim((-0.5, 0.1))

P.tick_params(axis='both', which='major', labelsize=20, size=8., width=1.5, pad=8.)
P.tick_params(axis='both', which='minor', labelsize=20, size=5., width=1.5, pad=8.)

P.tight_layout()
P.savefig("pub-w0wa-rainbow.png", transparent=True)
P.show()
