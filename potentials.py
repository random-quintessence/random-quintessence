#!/usr/bin/python
"""
Define various quintessence potentials.
"""
import numpy as np
import scipy.stats
from scipy.misc import factorial
from solve import DEModel

class Polynomial(DEModel):
    
    def __init__(self, N=1, p=1, clambda=0., symmetric=False,
                 log_prior=False, **kwargs):
        """
        Generic polynomial potential model.
        """
        DEModel.__init__(self, **kwargs)
        
        # Polynomial order
        self.N = N
        self.p = p
        self.clambda = clambda
        self.symmetric = symmetric
        self.paramnames = ['A', 'N', 'p', 'clambda']
        
        # Type of prior on initial field value
        self.log_prior = log_prior
    
    def V(self, p):
        """
        Dimensionless potential, P(x).
        """
        return self.func_V(p)
    
    def dVdp(self, p):
        """
        Dimensionless derivative of potential with respect to phi, dP/dx.
        """
        return self.func_dVdp(p)
    
    def c(self, n):
        """
        Deterministic coefficients.
        """
        return np.ones(self.N)
    
    def initial_scale(self):
        """
        Return range of random distribution that phi_i should be drawn from.
        """
        return [-1., 1.]
    
    def realise(self, xi=[0., 1.], xi_lambda=[0., 1.]):
        """
        Make a realisation of the random potential.
        """
        coeffs = np.zeros(self.N + self.p)
        
        # Realise polynomial coefficients
        n = np.arange(self.N)[::-1]
        coeffs[:self.N] = self.c(n) * np.random.normal(loc=xi[0], scale=xi[1], size=self.N)
        
        # Realise cosmological constant term
        coeff_lambda = self.clambda * np.random.normal( loc=xi_lambda[0], 
                                                        scale=xi_lambda[1])
        coeffs[-1] += coeff_lambda
        self.rho_lambda = self.A * coeff_lambda
        
        # Apply symmetry constraint
        if self.symmetric:
            coeffs[np.where(n % 2 == 1)] = 0.
        
        # Construct polynomial and its derivative
        self.func_V = np.poly1d(coeffs)
        self.func_dVdp = np.polyder(self.func_V)
        self.coeffs = coeffs
    
    def realise_scales(self, logA=None, N=None, Nq=None, p=None, pE=None, pD=None, 
                       logEF=None, logENP=None, logED=None, a=None, zeq=None, 
                       realise_potential=True):
        """
        Make a realisation of the various scale parameters that might be used 
        by a random potential:
        
        logA   : Overall amplitude of the potential
        N      : Order of polynomial
        Nq     : Order of polynomial for quantum corrections (EFT)
        p      : Leading order of polynomial
        pE     : Leading order of quantum corrections polynomial (EFT)
        pD     : Leading order of exponential series (moduli/extra dimensions)
        logEF  : epsilon_F, EFT mass suppression parameter (also used by axions)
        logENP : epsilon_NP, non-perturbative mass suppression parameter (axions)
        logED  : epsilon_D, extra dim. mass suppression parameter
        a      : Exponential suppression scale (moduli/extra dimensions)
        zeq    : Redshift of matter-radiation equality
        
        This method is generic; subclasses will only use some of the realisable 
        parameters.
        """
        if logA is not None:
            self.A = 10.**( np.random.uniform(low=logA[0], high=logA[1]) )
        if N is not None:
            self.N = np.random.randint(low=N[0], high=N[1]+1)
        if Nq is not None:
            self.Nq = np.random.randint(low=Nq[0], high=Nq[1]+1)
        if p is not None:
            self.p = np.random.randint(low=p[0], high=p[1]+1)
        if pE is not None:
            self.pE = np.random.randint(low=pE[0], high=pE[1]+1)
        if pD is not None:
            self.pD = np.random.randint(low=pD[0], high=pD[1]+1)
        if logEF is not None:
            self.eF = 10.**( np.random.uniform(low=logEF[0], high=logEF[1]) )
        if logENP is not None:
            self.eNP = 10.**( np.random.uniform(low=logENP[0], high=logENP[1]) )
        if logED is not None:
            self.eD = 10.**( np.random.uniform(low=logED[0], high=logED[1]) )
        if a is not None:
            self.aa = np.random.uniform(low=a[0], high=a[1])
        if zeq is not None:
            self.zeq = np.random.uniform(low=zeq[0], high=zeq[1])
        
        # Make a new realisation of the potential with the new parameters
        if realise_potential: self.realise()
    
    def realise_initial(self, kind='zero'):
        """
        Realise initial conditions on the field, phi, given the current 
        realisation of the potential.
        
        Parameters
        ----------
        kind : str
            Type of initial condition to apply on phidot. Choose from: 
            {zero, slowroll, tracking}
        
        Returns
        -------
        pi, pdoti : float
            Initial conditions on phi and phidot.
        """
        # phi_i
        scale = self.initial_scale()
        if not self.log_prior:
            # Uniform prior
            pi = np.random.uniform(low=scale[0], high=scale[1])
        else:
            # log-uniform prior
            sgn = +1. if (np.random.randint(low=-1, high=1) == 0) else -1.
            pi = sgn * 10.**np.random.uniform(low=scale[0], high=scale[1])
        
        # phidot_i
        if kind == 'tracking':
            pscale = np.sqrt( 2. * np.abs(self.A * self.V(pi)) )
            if not self.log_prior:
                pdoti = np.random.uniform(low=-pscale, high=+pscale)
            else:
                sgn = +1. if (np.random.randint(low=-1, high=1) == 0) else -1.
                pdoti = sgn * pscale * 10.**np.random.uniform(low=-3., high=0.)
        elif kind == 'slowroll':
            raise NotImplementedError("Slowroll not implemented yet.")
        else:
            pdoti = 0.
        return pi, pdoti
    
    def params(self):
        """
        Return a list of model parameters.
        """
        coeffs = np.zeros(30)
        coeffs[:self.coeffs.size] = self.coeffs
        params = np.concatenate(
            ([self.get_property_by_name(name) for name in self.paramnames], coeffs))
        names = self.paramnames + ["c%d" % i for i in range(coeffs.size)]
        return params, names
    
    def get_property_by_name(self, attr):
        """
        Get property of class by name.
        """
        for obj in [self]+self.__class__.mro():
            if attr in obj.__dict__:
                return obj.__dict__[attr]
        raise AttributeError


class Kac(Polynomial):
    def __init__(self, **kwargs):
        Polynomial.__init__(self, p=1, **kwargs)
        self.realise()
    def c(self, n): return 1.

class Weyl(Polynomial):
    def __init__(self, **kwargs):
        Polynomial.__init__(self, p=1, **kwargs)
        self.realise()
    def c(self, n): return 1. / np.sqrt(factorial(n))

class Monomial(Polynomial):
    def __init__(self, A, p, clambda=0., mean=0., std=1., symmetric=False, **kwargs):
        Polynomial.__init__(self, A=A, p=p, N=1, clambda=clambda, **kwargs)
        self.realise()
    def c(self, n): return 1.
    def realise(self, xi=[1., 1e-20], xi_lambda=[0., 1.]): # Hack to keep xi=1
        self.N = 1
        Polynomial.realise(self, xi=xi, xi_lambda=xi_lambda)
    def initial_scale(self):
        if float(self.clambda) == 0:
            return [0., 4.] # Odd fns. will fail anyway for phi_i < 0
        else:
            return [-4., 4.]

class MonomialDeterministic(Polynomial):
    def __init__(self, A, p, **kwargs):
        Polynomial.__init__(self, A=A, p=p, N=1, clambda=0., **kwargs)
        self.A = A
        self.p = p
        self.func_V = self.V
        self.func_dVdp = self.dVdp
    def realise(self): pass
    def V(self, x): return self.A * x**self.p
    def dVdp(self, x): return self.A * self.p * x**(self.p-1)
    def c(self, n): return 1.

class ShiftSymmEFT(Polynomial):
    def __init__(self, A, Nq, pE, eF, clambda=0.):
        self.Nq = Nq; self.pE = pE; self.eF = eF
        Polynomial.__init__(self, A=A, N=Nq+pE, p=0, clambda=clambda, symmetric=False)
        self.paramnames = ['A', 'Nq', 'pE', 'eF', 'clambda']
        self.realise()
    
    def realise(self, **kwargs):
        # Test validity of inputs
        assert self.pE > 4. # Requires pE > 4 (describes higher-order terms only)
        assert self.eF < 1. # Requires eF < 1 (for convergence)
        
        # Update polynomial order
        self.N = self.Nq + self.pE
        self.p = 0
        Polynomial.realise(self, **kwargs)
    
    def c(self, n):
        cc = np.zeros(self.N)
        cc[np.where(n==2)] = 1.
        cc[np.where(n==4)] = 1.
        cc[:self.N-self.pE] = 1. # eF folded into definition of V(p)
        return cc
    
    def initial_scale(self):
        return [-1./self.eF, 1./self.eF]
    
    def V(self, p):
        return self.func_V(p*self.eF)
        
    def dVdp(self, p):
        return self.eF*self.func_dVdp(p*self.eF)

"""
class ShiftSymmEFTOld(Polynomial):
    def __init__(self, A, Nq, pE, eF, clambda=0.):
        print "ShiftSymmEFT(): WARNING: Obsolete."
        self.Nq = Nq; self.pE = pE; self.eF = eF
        Polynomial.__init__(self, A=A, N=Nq+pE, p=0, clambda=clambda, symmetric=False)
        self.paramnames = ['A', 'Nq', 'pE', 'eF', 'clambda']
        self.realise()
    
    def realise(self, **kwargs):
        # Test validity of inputs
        assert self.pE > 4. # Requires pE > 4 (describes higher-order terms only)
        assert self.eF < 1. # Requires eF < 1 (for convergence)
        
        # Update polynomial order
        self.N = self.Nq + self.pE
        self.p = 0
        Polynomial.realise(self, **kwargs)
    
    def c(self, n):
        cc = np.zeros(self.N)
        cc[np.where(n==2)] = 1.
        cc[np.where(n==4)] = 1.
        cc[:self.N-self.pE] = self.eF**(n[:self.N-self.pE] - self.pE)
        cc *= self.eF**n # Extra fine-tuning, to ensure V ~ O(1)
        return cc
    
    def initial_scale(self):
        return [-1./self.eF, 1./self.eF]
"""

class Axion(Polynomial):
    
    def __init__(self, N=1, eNP=0., eF=0., clambda=0., **kwargs):
        DEModel.__init__(self, **kwargs)
        self.N = N
        self.eF = eF
        self.eNP = eNP
        self.clambda = clambda
        self.paramnames = ['A', 'N', 'eF', 'eNP', 'clambda']
    
    def V(self, p):
        f = np.sum([ self.coeffs[n-2] * self.eNP**(n-1) * np.cos(n*self.eF*p) 
                     for n in range(2, 2 + self.N) ], axis=0)
        f += 1. + np.cos(self.eF * p) + self.clambda*self.coeff_lambda
        return f
        
    def dVdp(self, p):
        f = np.sum([ -n * self.coeffs[n-2] * self.eNP**(n-1) * np.sin(n*self.eF*p) 
                     for n in range(2, 2 + self.N) ], axis=0)
        f += -np.sin(self.eF * p)
        return f * self.eF
        
    def c(self, n): pass
    
    def initial_scale(self): return [-np.pi/self.eF, np.pi/self.eF]
    
    def realise(self, xi=[0., 1.], xi_lambda=[0., 1.]):
        self.coeffs = np.random.normal(loc=xi[0], scale=xi[1], size=self.N)
        # Realise cosmological constant term
        self.coeff_lambda = self.clambda * np.random.normal( loc=xi_lambda[0], 
                                                             scale=xi_lambda[1])
        self.rho_lambda = self.A * self.coeff_lambda

class ExtraDimensions(Polynomial):
    
    def __init__(self, N=1, a=0., eD=0., pD=1, clambda=0., **kwargs):
        DEModel.__init__(self, **kwargs)
        self.N = N
        self.aa = a
        self.eD = eD
        self.pD = pD
        self.clambda = clambda
        self.paramnames = ['A', 'N', 'aa', 'eD', 'pD', 'clambda']
    
    def V(self, p):
        f = np.sum([ self.coeffs[n] * self.eD**n * np.exp(self.aa*(self.pD - n)*p) 
                     for n in range(0, self.N) ], axis=0)
        f += self.clambda*self.coeff_lambda
        return f
        
    def dVdp(self, p):
        f = np.sum([ self.coeffs[n] * self.eD**n * self.aa*(self.pD - n) * \
                         np.exp(self.aa*(self.pD - n)*p) 
                     for n in range(0, self.N) ], axis=0)
        return f
        
    def c(self, n): pass
    
    def initial_scale(self): return [-1., 1.]
    
    def realise(self, xi=[0., 1.], xi_lambda=[0., 1.]):
        self.coeffs = np.random.normal(loc=xi[0], scale=xi[1], size=self.N)
        # Realise cosmological constant term
        self.coeff_lambda = self.clambda * np.random.normal( loc=xi_lambda[0], 
                                                             scale=xi_lambda[1])
        self.rho_lambda = self.A * self.coeff_lambda
        
