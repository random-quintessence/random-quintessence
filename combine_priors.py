#!/usr/bin/python
"""
Make combined histogram of priors.
"""
import numpy as np
#import pylab as P
#import matplotlib.cm as cm
#import cProfile
import sys

NBINS = 500 # No. bins in each direction
WAMAX = 5.
W0MIN = -2.
W0MAX = 1.

fom = float(sys.argv[1])

# Load Fisher matrix for Euclid
pw0 = 19; pwa = 20
covmat = np.genfromtxt("../bao21cm/euclid_covmat.dat")
c11 = covmat[pw0,pw0]
c22 = covmat[pwa,pwa]
c12 = covmat[pw0,pwa]
cov0 = np.array([[c11, c12], [c12, c22]])

# Load data files
# ZERO, NO LAMBDA, CUT
t_eft = np.load("eft.npy")
t_axion = np.load("axion.npy")
t_extra = np.load("extradim.npy")


def loglike(xy, x0, y0, Cinv, detC):
    """
    Gaussian likelihood for 2 parameters (w0, wa).
    """
    #xx = np.atleast_2d([x - x0, y - y0])
    xy[0] -= x0; xy[1] -= y0
    xx = xy
    logL = -0.5 * np.einsum("imn,imn->mn", xx, np.einsum("ij,ikl", Cinv, xx))
    xy[0] += x0; xy[1] += y0
    return logL - np.log(2.*np.pi * np.sqrt(detC))

def relative_entropy(logL, q):
    """
    Calculate relative entropy, dS = log A + Sum_k (A L_k q_k log L_k),
    where A is the normalisation for the posterior P_k = A L_k q_k, and 
    L_k is the likelihood and q_k is the prior density.
    """
    Q = 1. / np.sum(q) # Prior normalisation
    Pk = np.exp(logL) * (Q * q)
    A = 1. / np.sum(Pk) # Posterior normalisation
    dS = A * np.sum(Pk * logL) + np.log(A)
    return dS

def rel_entropy_for_fom(cov, Q, XY, fid_w0, fid_wa, Cinv, detC):
    """
    Get relative entropy for a given FOM.
    """
    # Calculate likelihood and posterior normalisation
    logL = loglike(XY, fid_w0, fid_wa, Cinv, detC)

    # Calculate relative entropy
    dS = relative_entropy(logL, Q)
    return dS

def combined_prior(dat):
    """
    Get normalised combined prior.
    """
    # Calculate combined prior on all physical models
    combined = 0
    for d in dat:
        w0 = d[0]; wa = d[1]
        q, x, y = np.histogram2d(w0, wa, bins=NBINS, normed=True, 
                                 range=((W0MIN, W0MAX), (-WAMAX, WAMAX)))
        combined += q.T
    Q = 1. / np.sum(combined)
    
    # Get grid points
    xc = [0.5*(x[i+1] + x[i]) for i in range(x.size-1)]
    yc = [0.5*(y[i+1] + y[i]) for i in range(y.size-1)]
    X, Y = np.meshgrid(xc, yc)
    
    return Q * combined, X, Y

# Combine physical models into one prior
Q, X, Y = combined_prior([t_eft, t_axion, t_extra])
XY = np.atleast_2d([X, Y]) # Precompute this (saves a lot of time!)

# Scale covmat for desired FOM
fom0 = 1. / np.sqrt(np.linalg.det(cov0))
cov = cov0 * (fom0 / fom)

# Pre-calculate Cinv and det(C)
theta = 0.
R = np.array( [[np.cos(theta), -np.sin(theta)], [np.sin(theta), np.cos(theta)]] )
Cinv = np.dot(R, np.dot(np.linalg.inv(cov), R.T))
detC = np.linalg.det(cov)

# Calculate dS on sample points for likelihood (w0, wa) origin
THRES = 1e-7 #5e-5
iw0, iwa = np.where(Q > THRES) # Only take (w0, wa) points where Q > threshold
print "Points above threshold:", iw0.size, "/", np.where(Q > 0.)[0].size

QdS_integ = 0.
Q_integ = 0.
for k in xrange(iw0.size):
    #if k % 20 == 0: print k, "/", iw0.size
    w0 = X[iw0[k],iwa[k]]
    wa = Y[iw0[k],iwa[k]]
    qq = Q[iw0[k],iwa[k]]
    
    # Calculate rel. entropy and add to integral
    dS = rel_entropy_for_fom(cov, Q, XY, w0, wa, Cinv, detC)
    QdS_integ += qq * dS
    Q_integ += qq

#print "int(dS . Q):", QdS_integ
#print "int(Q):", Q_integ
#print "<dS>:", QdS_integ / Q_integ
print "***", fom, THRES, iw0.size, QdS_integ, Q_integ, QdS_integ / Q_integ
