#!/usr/bin/python
"""
Examine output for one run
"""
import numpy as np
import pylab as P

# Get labels
f = open("data/eft-test-paramnames.txt")
lbls = f.readlines()[0].split(",")
f.close()

# Open paramfile and trim failed models
d = np.load("data/eft-test-params-0.npy").T
bad = d.T[np.where(d[0] == 0.)].T
good = d.T[np.where(np.logical_not(d[0] == 0.))].T

print "Bad: ", bad.shape
print "Good:", good.shape

P.plot(good[0], good[1], 'r.')
P.axvline(-1., color='k', ls='dotted')
P.axhline(0., color='k', ls='dotted')
P.xlim((-2., 1.))
P.ylim((-21., 21.))

P.show()
