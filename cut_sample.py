#!/usr/bin/python
"""
Apply cuts to sample and output clean sample set.
"""
import numpy as np
import pylab as P
import matplotlib.cm as cm
import matplotlib.patches
import sys, os

# Get name from commandline
try:
    NAME = sys.argv[1]
except:
    print "Expected one argument: name (weyl, kac, eft, etc.)"
    sys.exit()


FIX = False
if 'kac-lambda' in NAME: ROWLEN = 44
if 'weyl-lambda' in NAME: ROWLEN = 44
if 'mono-lambda' in NAME: ROWLEN = 44
if 'eft-lambda' in NAME: ROWLEN = 45

# Cuts to apply
OMEGAQ = [0.6, 0.8]
H = [0.6, 0.8]
OMEGA_EDE = 0.042 # From Joudaki (2012)

def combine_samples(root, basename):
    """
    Load all samples for a given model.
    """
    dat = []
    name = "%s-params" % basename
    for f in os.listdir(root):
        if name in f:
            # Load data
            print "\tLoading %s..." % f
            _dat = np.load("%s/%s" % (root,f))
            # FIXME: Deal with malformed runs
            if FIX:
                dat.append([row if len(row)==ROWLEN else np.zeros(ROWLEN) for row in _dat])
            else:
                dat.append(np.load("%s/%s" % (root,f)))
    return np.concatenate(dat).T

print " * Loading data files"
dat = combine_samples("data/", NAME)

print dat.shape

# Apply cuts
print " * Applying observational cuts"
cuts = []
c1 = np.logical_and(dat[3] >= OMEGAQ[0], dat[3] <= OMEGAQ[1]) # Omega_Q
c2 = np.logical_and(dat[2] >= H[0], dat[2] <= H[1]) # h
c3 = (dat[7] < OMEGA_EDE) # Early dark energy

# OLD VERSION (no cc)
#c1 = np.logical_and(dat[3] >= OMEGAQ[0], dat[3] <= OMEGAQ[1]) # Omega_Q
#c2 = np.logical_and(dat[2] >= H[0], dat[2] <= H[1]) # h
#c3 = (dat[6] < OMEGA_EDE) # Early dark energy

msk = np.where( c1 & c2 & c3 )

print "  -- Retained fraction: %4.2f%% (%d / %d samples)" % (100.*float(msk[0].size) / float(dat[0].size), msk[0].size, dat[0].size)
print "     Omega_Q cut: %4.2f%% (%d / %d samples)" % (100.*float(np.sum(c1)) / float(dat[0].size), np.sum(c1), dat[0].size)
print "     H_0 cut: %4.2f%% (%d / %d samples)" % (100.*float(np.sum(c2)) / float(dat[0].size), np.sum(c2), dat[0].size)
print "     EDE cut: %4.2f%% (%d / %d samples)" % (100.*float(np.sum(c3)) / float(dat[0].size), np.sum(c3), dat[0].size)

# Save cuts
outname = NAME
print " * Saving to %s.npy" % outname
cut_dat = [dat[i][msk] for i in range(dat.shape[0])]
cut_dat = np.array(cut_dat)
np.save(outname, cut_dat)
