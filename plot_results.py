#!/usr/bin/python
"""
Plot results of a run.
"""
import numpy as np
import pylab as P

w0, wa, h, ol, om, orad = np.load("values.npy").T


P.subplot(211)
P.plot(w0, wa, 'b.')
P.xlim((-2., 2.))
P.ylim((-20., 20.))
P.xlabel("w0")
P.ylabel("wa")
P.plot([-1.,], [0.,], 'kx')


P.subplot(212)
P.plot(om, ol, 'r.')
P.xlim((0., 2.))
P.ylim((-2., 2.))
P.xlabel("Omega_M")
P.ylabel("Omega_Q")
P.plot([0.3,], [0.7,], 'kx')

P.show()
