#!/usr/bin/python
"""
Plot w0-wa for a bunch of models.
"""
import numpy as np
import pylab as P
import matplotlib.cm as cm
import matplotlib.patches
import matplotlib.ticker
import scipy.ndimage
import sys
sys.path.append("/home/phil/oslo/bao21cm/")
import baofisher

colours = [ ['#6B6B6B', '#BDBDBD'],
            ['#CC0000', '#F09B9B'],
            ['#1619A1', '#B1C9FD'],
            ['#5B9C0A', '#BAE484'],
            ['#FFB928', '#FFEA28'], ]

nice_colours = ['#CC0000', '#1619A1', '#5B9C0A', '#FFB928', '#750075']

def get_centroids(vals):
	"""Get bin centroids"""
	cent = []
	for i in range(len(vals)-1):
		cent.append(0.5*(vals[i]+vals[i+1]))
	return cent

def combine_samples(root, N):
    """
    Load all samples for a given model.
    """
    dat = []
    for i in range(N):
        dat.append( np.load("%s-%d.npy" % (root, N)) )
    return np.concatenate(dat).T

# Load pre-cut data
# 0:w0, 1:wa, 2:h, 3:oq, 4:om, 5:orad, ...
d_eft = np.load("eft-w0wa.npy")
d_eft_lambda = np.load("eft-lambda-w0wa.npy")
d_eftnew = np.load("data/eft-test-params-99.npy") # FIXME

#P.plot(d_eft[0], d_eft[1], 'b.', alpha=0.5)
#P.plot(d_eft_lambda[0], d_eft_lambda[1], 'r.', alpha=0.5)
P.plot(d_eftnew[0], d_eftnew[1], 'g.', alpha=0.5)

# Euclid error ellipse
pw0 = 19; pwa = 20
x = -1.; y = 0.
covmat = np.genfromtxt("../bao21cm/euclid_covmat.dat")
#transp = [1., 0.85]
transp = [0.5, 0.3]
w, h, ang, alpha = baofisher.ellipse_for_fisher_params(pw0, pwa, None, Finv=covmat)
ellipses = [matplotlib.patches.Ellipse(xy=(x, y), width=alpha[kk]*w, 
            height=alpha[kk]*h, angle=ang, fc=colours[0][kk], 
            ec=colours[0][0], lw=1.5, alpha=transp[kk]) for kk in [1,0]]
for e in ellipses: P.gca().add_patch(e)
P.plot(x, y, 'kx', markersize=8., mew=1.8)

P.axhline(0., color='k', lw=1.5, alpha=0.2)
P.axvline(-1., color='k', lw=1.5, alpha=0.2)

# Axis labels
#P.gca().xaxis.set_major_locator(matplotlib.ticker.MultipleLocator(0.1))
#P.gca().xaxis.set_minor_locator(matplotlib.ticker.MultipleLocator(0.05))

P.xlabel("$w_0$", fontdict={'fontsize':'xx-large'}, labelpad=15.)
P.ylabel("$w_a$", fontdict={'fontsize':'xx-large'}, labelpad=15.)
P.xlim((-2., 1.))
P.ylim((-30., 30.))

P.tick_params(axis='both', which='major', labelsize=20, size=8., width=1.5, pad=8.)
P.tick_params(axis='both', which='minor', labelsize=20, size=5., width=1.5, pad=8.)

P.tight_layout()
#P.savefig("pub-w0wa-tracks.pdf", transparent=True)
P.show()
