#!/usr/bin/python
"""
Monte Carlo through a load of random realisations of a model (without MPI).
"""
import numpy as np
from potentials import *
import sys

BASE_SEED = 500

# Get commandline arguments
try:
    myid = int(sys.argv[1])
    mtype = sys.argv[2]
    clambda = True if 't' in str(sys.argv[3]).lower() else False
    inikind = str(sys.argv[4]).lower()
    Nsamp = int(sys.argv[5])
    log_prior = False
    if len(sys.argv) > 6:
        log_prior = True if 't' in str(sys.argv[6]).lower() else False
except:
    print "Expects arguments: id(int), model_type(str), use_lambda(bool), phidot_ini_type(str, 'zero'/'tracking'), nsamp(int), [optional:log_prior(bool)]"
    sys.exit()
basename = "data/%s%s%s%s" % (mtype, "-tracking" if inikind == 'tracking' else "", 
                                     "-lambda" if clambda else "",
                                     "-logprior" if log_prior else "")
print "-"*50
print "ID:", myid
print "Model Type:", mtype
print "Lambda:", clambda
print "Ini. Type:", inikind
print "Samples:", Nsamp
print "Log prior:", log_prior
print "Base filename:", basename
print "-"*50

# Random seed depends on worker
np.random.seed(BASE_SEED + myid)

# Prepare empty files for appending evolution arrays
fname_evol = "%s-evol-%d" % (basename,myid)
fname_wz = "%s-wz-%d" % (basename,myid)
f = open(fname_evol+".npy", 'w'); f.close()
f = open(fname_wz+".npy", 'w'); f.close()

# Setup integrator
X0 = 1e0 # a0 / a_ref (also defined in solve.py)
EFOLDS_BEFORE_EQ = 1e-2

# Define generic priors on potential parameters
priors = { 'logA':[-1.,1.], 'N':None, 'Nq':None, 'p':None, 'pE':None, 
           'logEF':None, 'zeq':None }
priors['zeq'] = [2000, 4000] # Matter-radiation equality

# Define model and appropriate priors
if mtype == 'mono':
    m = Monomial(A=1., clambda=clambda, p=1, log_prior=log_prior)
    priors['p'] = [1, 7]
elif mtype == 'kac':
    m = Kac(A=1., clambda=clambda, log_prior=log_prior)
    priors['N'] = [10, 20]
elif mtype == 'weyl':
    m = Weyl(A=1., clambda=clambda, log_prior=log_prior)
    priors['N'] = [10, 20]
elif mtype == 'eft':
    m = ShiftSymmEFT(A=1., clambda=clambda, Nq=5, pE=5, eF=0.1, log_prior=log_prior)
    priors['Nq'] = [5, 10]
    priors['pE'] = [5, 10]
    priors['logEF'] = [-3., -1.]
elif mtype == 'axion':
    m = Axion(A=1., clambda=clambda, N=5, eF=0.1, eNP=0.1, log_prior=log_prior)
    priors['N'] = [10, 20]
    priors['logEF'] = [-3., np.log10(0.9)]
    priors['logENP'] = [-3., np.log10(0.9)]
elif mtype == 'extradim':
    m = ExtraDimensions(A=1., clambda=clambda, N=5, a=0., eD=0.1, pD=1, 
                        log_prior=log_prior)
    priors['N'] = [10, 20]
    priors['a'] = [0., 1.]
    priors['logED'] = [-3., np.log10(0.9)]
    priors['pD'] = [1, 5]
else:
    raise ValueError("Invalid model type specified: %s." % mtype)

# Define time variable (assumed to cover a large enough dynamic range for any model)
t = np.logspace(-2., 3., 10000)
t = np.concatenate(([0.,], t)) # Add leading zero

# Produce set of samples for many randomly-realised models
vals = []; evol_idxs = []
for i in range(Nsamp):
    if i % 50 == 0: print "Model %d from worker %d" % (i, myid)
    #print i
    
    # Update potential/initial conditions
    m.realise_scales(**priors)
    xi = EFOLDS_BEFORE_EQ * X0 / (1. + m.zeq) # Initial scalefactor
    pi, pdoti = m.realise_initial(kind=inikind)
    yi = [xi, None, pi, pdoti] # xdoti will be calculated from Friedmann eqn.
    
    # Integrate ODEs and plot functions
    y = x, xdot, p, pdot = m.integrate_friedmann(t, yi)
    
    # Analyse solution and collect parameter values
    cosmo_today = m.analyse(t, y)
    params, paramnames = m.params()
    params = np.concatenate((params, [pi, pdoti]))
    paramnames += ["phi_i", "phidot_i"]
    paramnames = ['w0', 'wa', 'h', 'oq', 'ol', 'om', 'orad', 'f_ede', 'zeq'] + paramnames
    # w0, wa, h, oq, ol, om, orad, f_ede, zeq
    vals.append( np.concatenate((cosmo_today, params)) )
    
    # Apply cuts; append to evol_fns if passes cuts
    oq = cosmo_today[3]; hh = cosmo_today[2]
    if (oq >= 0.6) and (oq <= 0.8) and (hh > 0.6) and (hh < 0.8):
        evol_idxs.append(i) # Keep track of idxs that pass the cut
        
        # Append evolution arrays directly to file (don't keep in memory)
        f1 = open(fname_evol+".npy", 'a+b')
        f2 = open(fname_wz+".npy", 'a+b')
        f1.seek(0, 2); f2.seek(0, 2) # Seek to end of file
        np.save(f1, y); np.save(f2, m.w(y))
        f1.close(); f2.close()

print "\t*", myid, "saving results..."
np.save("%s-params-%d" % (basename,myid), vals)
np.save("%s-evol-idxs-%d" % (basename,myid), np.array(evol_idxs))
if myid == 0:
    f = open("%s-paramnames.txt" % basename, 'w')
    f.write(",".join(paramnames))
    f.close()

# Load evol. arrays from multi-block files and repack into single block in each file
#np.save("%s-evol-%d" % (basename,myid), np.array(evol_fns))
#np.save("%s-wz-%d" % (basename,myid), np.array(wz))

def load_multiblock(fname):
    """
    Load multi-block file and return combined array.
    """
    f = open(fname+".npy", 'rb')
    ctr = 0; end = False; dat = []
    while not end:
        ctr += 1
        try:
            row = np.load(f)
            dat.append(row)
        except:
            f.close()
            break
        if ctr > 1e7:
            print "load_multiblock(): Counter exceeded 10 million. Stopping."
            end = True
            f.close()
    return np.array(dat)

print " * Saving evol. arrays..."
np.save(fname_evol, load_multiblock(fname_evol))
np.save(fname_wz, load_multiblock(fname_wz))


# Root should combine all the results now
#if myid == 0:
#    dat = [np.load("%s-%d.npy"%(basename,i)) for i in range(size)]
#    all_vals = np.concatenate(dat)
#    np.save("values", all_vals)
#    print "\t* Saved combined results."
