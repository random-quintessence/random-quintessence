#!/usr/bin/python
"""
Plot w0-wa mosaic for a bunch of models.
"""
import numpy as np
import pylab as P
import matplotlib.cm as cm
import matplotlib.patches
import matplotlib.ticker

i = 0
#root = "data/weyl-lambda"
#root = "data/kac-lambda"
#root = "data/eft-lambda"
#root = "data/mono-lambda"

ax = P.subplot(111)

zz = []; HH = []; ww = []
for i in range(10):
    a, adot = np.load("%s-evol-%d.npy" % (root,i))[:,:2,:]
    wz = np.load("%s-wz-%d.npy" % (root,i))
    Hz = adot / a
    z = 1./a - 1.
    
    zz.append(z)
    ww.append(wz)
    HH.append(Hz)
    
    #for j in range(a.shape[0]):
        #ax1.plot(z[j], wz[j], 'k-', alpha=0.1)
        #ax2.plot(z[j], Hz[j]/(1.+z[j])**1.5, 'k-', alpha=0.1)


xmin, xmax = (0., 2.)
ymin, ymax = (-0.2, 0.2)

# Calculate 2D density
f, xe, ye = np.histogram2d(zz.flatten(), ww.flatten(), bins=100, 
                           range=((xmin, xmax), (ymin, ymax)) )
ax.matshow(f.T, extent=[xmin, xmax, ymin, ymax], origin='lower', aspect=10.)

P.savefig("fnz.png")
#P.show()
