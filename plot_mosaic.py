#!/usr/bin/python
"""
Plot w0-wa mosaic for a bunch of models.
"""
import numpy as np
import pylab as P
import matplotlib.cm as cm
import matplotlib.patches
import matplotlib.ticker
import scipy.ndimage
import sys
sys.path.append("/home/phil/oslo/bao21cm/")
import baofisher

"""
colours = [ ['#6B6B6B', '#BDBDBD'],
            ['#CC0000', '#F09B9B'],
            ['#1619A1', '#B1C9FD'],
            ['#5B9C0A', '#BAE484'],
            ['#FFB928', '#FFEA28'], ]

nice_colours = ['#CC0000', '#1619A1', '#5B9C0A', '#FFB928']
"""

PRECUT = False # Whether I'm plotting pre-cut values or not
NBINS = 100 # 50
ASPECT = 0.4 # Aspect ratio of imshow's

if PRECUT:
    W0MIN, W0MAX = (-1.57, -0.43)
    WAMIN, WAMAX = (-1.07, 1.07)
else:
    W0MIN, W0MAX = (-1.1, -0.7)
    WAMIN, WAMAX = (-0.5, 0.5)

def combine_samples(root, N):
    """
    Load all samples for a given model.
    """
    dat = []
    for i in range(N):
        dat.append( np.load("%s-%d.npy" % (root, N)) )
    return np.concatenate(dat).T

# Load pre-cut data
# 0:w0, 1:wa, 2:h, 3:oq, 4:om, 5:orad, ...

# ZERO, NO LAMBDA, CUT
t_mono = np.load("mono.npy")
t_kac = np.load("kac.npy")
t_weyl = np.load("weyl.npy")
t_eft = np.load("eft.npy")
t_axion = np.load("axion.npy")
t_extra = np.load("extradim.npy")

# TRACKING, NO LAMBDA
#l_mono = np.load("mono-tracking.npy")
#l_kac = np.load("kac-tracking.npy")
#l_weyl = np.load("weyl-tracking.npy")
#l_eft = np.load("eft-tracking.npy")
#l_axion = np.load("axion-tracking.npy")

# ZERO + LAMBDA, CUT
m_mono = np.load("mono-lambda.npy")
m_kac = np.load("kac-lambda.npy")
m_weyl = np.load("weyl-lambda.npy")
m_eft = np.load("eft-lambda.npy")
m_axion = np.load("axion-lambda.npy")
m_extra = np.load("extradim-lambda.npy")

# TRACKING + LAMBDA
#m_mono = np.load("mono-tracking-lambda.npy")
#m_kac = np.load("kac-tracking-lambda.npy")
#m_weyl = np.load("weyl-tracking-lambda.npy")
#m_eft = np.load("eft-tracking-lambda.npy")
#m_axion = np.load("axion-tracking-lambda.npy")

# ZERO + LAMBDA, UNCUT
b_mono = np.load("mono-lambda-w0wa.npy")
b_kac = np.load("kac-lambda-w0wa.npy")
b_weyl = np.load("weyl-lambda-w0wa.npy")
b_eft = np.load("eft-lambda-w0wa.npy")
b_axion = np.load("axion-lambda-w0wa.npy")
b_extra = np.load("extradim-lambda-w0wa.npy")

dat = [ b_kac, b_weyl, b_mono, b_eft, b_axion, b_extra,
        m_kac, m_weyl, m_mono, m_eft, m_axion, m_extra,
        t_kac, t_weyl, t_mono, t_eft, t_axion, t_extra ]

labels = ["Kac", "Weyl", "Mono", "EFT", "Axion", "Moduli"]
axes = [ P.subplot(3,6,i+1) for i in range(18) ]

cmap = [cm.Reds, cm.Oranges, cm.YlOrBr, cm.Greens, cm.Blues, cm.Purples][::-1]


# Add figure text
P.figtext(0.885, 0.325, r"No cut, $\Lambda \neq 0$") # Lower
P.figtext(0.94, 0.58, r"$\Lambda \neq 0$") # Middle
P.figtext(0.94, 0.84, r"$\Lambda = 0$") # Upper


l0 = 0.08
b0 = 0.07
ww = 0.90 / 6.
hh = 1. / 3.

j = 0
for i in range(len(dat)):
    w0 = dat[i][0]; wa = dat[i][1]
    f, x, y = np.histogram2d(w0, wa, bins=NBINS, range=((W0MIN, W0MAX), (WAMIN, WAMAX)))
    ms = axes[i].matshow(np.log10(f).T, extent=[W0MIN, W0MAX, WAMIN, WAMAX], origin='lower', aspect=0.4, cmap=cmap[i%len(labels)]) # 0.4
    
    print f.T.shape
    print P.gcf().get_dpi()
    
    # Reference Lambda lines
    axes[i].axhline(0., color='k', lw=1.5, alpha=0.2)
    axes[i].axvline(-1., color='k', lw=1.5, alpha=0.2)

    # Axis labels
    if PRECUT:
        axes[i].xaxis.set_major_locator(matplotlib.ticker.MultipleLocator(0.5))
        axes[i].xaxis.set_minor_locator(matplotlib.ticker.MultipleLocator(0.25))
        axes[i].yaxis.set_major_locator(matplotlib.ticker.MultipleLocator(0.5))
        axes[i].yaxis.set_minor_locator(matplotlib.ticker.MultipleLocator(0.25))
    else:
        axes[i].xaxis.set_major_locator(matplotlib.ticker.MultipleLocator(0.2))
        axes[i].xaxis.set_minor_locator(matplotlib.ticker.MultipleLocator(0.1))
        axes[i].yaxis.set_major_locator(matplotlib.ticker.MultipleLocator(0.2))
        axes[i].yaxis.set_minor_locator(matplotlib.ticker.MultipleLocator(0.1))
    
    axes[i].set_xlim((W0MIN, W0MAX))
    axes[i].set_ylim((WAMIN, WAMAX))
    
    # Increment row counter
    if i > (len(labels)-1): j = 1
    if i > (2*len(labels)-1): j = 2
    
    # Title for each column
    if j == 2:
        axes[i].set_xlabel(labels[i%len(labels)], fontsize=18)
        axes[i].xaxis.set_label_position('top')
    
    # Remove tick labels for all but bottom left subplot
    if i == 0:
        axes[i].tick_params(axis='both', which='major', labelleft='on', labeltop='off', labelbottom='on', labelsize=14, size=3., width=1.3, pad=8.)
    else:
        axes[i].tick_params(axis='both', which='major', labelleft='off', labeltop='off', labelbottom='off', top='off', size=3., width=1.3, pad=8.)
    
    axes[i].tick_params(axis='both', which='minor', size=3., width=1.3, pad=8.)
    
    # Move subplots
    # pos = [[x0, y0], [x1, y1]]
    if PRECUT:
        RESCALE = 0.4
    else:
        RESCALE = 0.3085 #0.32 #0.2665
    scaling = RESCALE/((W0MAX - W0MIN) / (WAMAX - WAMIN)) # Scaling factor for height
    axes[i].set_position([l0+ww*(i%len(labels)), b0 + scaling*hh*j, ww, hh])
    
# Legend
#labels = [labels[k] for k in range(len(labels))]
#lines = [ matplotlib.lines.Line2D([0.,], [0.,], lw=8.5, color=colours[k][0], alpha=0.65) for k in range(len(labels))]

#P.gcf().legend((l for l in lines), (name for name in labels), prop={'size':'medium'}, bbox_to_anchor=[0.95, 0.95])

#P.xlabel("$w_0$", fontdict={'fontsize':'xx-large'}, labelpad=15.)
#P.ylabel("$w_a$", fontdict={'fontsize':'xx-large'}, labelpad=15.)

axes[0].set_xlabel("$w_0$", fontsize=16)
axes[0].set_ylabel("$w_a$", fontsize=16, labelpad=2.)

#P.tight_layout()
if PRECUT:
    P.gcf().set_size_inches(10.,4.)
    P.savefig('pub-mosaic-uncut.pdf', transparent=False)
else:
    P.gcf().set_size_inches(12.,7.)
    #P.savefig('pub-mosaic.pdf', transparent=False)
    P.savefig('pub-mosaic-three.pdf', transparent=False)
#P.show()
